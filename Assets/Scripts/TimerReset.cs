﻿using UnityEngine;
using System.Collections;

public class TimerReset : MonoBehaviour
{
	public float ResetTime = 5f;

	void Start()
	{
		Invoke("Reset", ResetTime);
	}

	void Reset()
	{
		//
		PlayerControl.Reset();
		QuestManager.Reset();
		GameData.PlayerPosX = -4.6f;
		GameData.PlayerPosY = -1.44f;
		GameData.ActiveScene = 1;

		Application.LoadLevel(1);
	}
}